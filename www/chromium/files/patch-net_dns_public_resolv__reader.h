--- net/dns/public/resolv_reader.h.orig	2022-02-07 13:39:41 UTC
+++ net/dns/public/resolv_reader.h
@@ -34,4 +34,4 @@ NET_EXPORT absl::optional<std::vector<IPEndPoint>> Get
 
 }  // namespace net
 
-#endif  // NET_DNS_PUBLIC_RESOLV_READER_H_
\ No newline at end of file
+#endif  // NET_DNS_PUBLIC_RESOLV_READER_H_
