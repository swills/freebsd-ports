--- gui/core/utils/TimeUtil.h.orig	2022-02-16 14:27:26 UTC
+++ gui/core/utils/TimeUtil.h
@@ -2,6 +2,8 @@
 #ifndef ES_CORE_UTILS_TIME_UTIL_H
 #define ES_CORE_UTILS_TIME_UTIL_H
 
+#include <ctime>
+
 #include <string>
 
 namespace Utils
