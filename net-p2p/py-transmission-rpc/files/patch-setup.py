--- setup.py.orig	2021-09-29 06:26:44 UTC
+++ setup.py
@@ -8,7 +8,7 @@ package_data = \
 {'': ['*']}
 
 install_requires = \
-['requests>=2.23.0,<3.0.0', 'typing_extensions>=3.7.4.2,<4.0.0.0']
+['requests>=2.23.0,<3.0.0', 'typing_extensions>=3.7.4.2,<4.0.2.0']
 
 extras_require = \
 {'docs': ['sphinx==4.2.0', 'sphinx-rtd-theme==1.0.0']}
